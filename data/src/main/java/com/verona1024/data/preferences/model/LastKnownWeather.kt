package com.verona1024.data.preferences.model

data class LastKnownWeather(
        var coord: Coord? = null,
        var sys: Sys? = null,
        var weather: Weather? = null,
        var main: Main? = null,
        var wind: Wind? = null,
        var rain: Rain? = null,
        var clouds: Clouds? = null,
        var dt: Double? = null,
        var id: Double? = null,
        var name: String? = null,
        var cod: Double? = null
) {

    data class Coord(
            var lon: Double? = null,
            var lat: Double? = null
    )


    data class Rain(
            var h: Double? = null
    )


    data class Weather(
            var id: Double? = null,
            var main: String? = null,
            var description: String? = null,
            var icon: String? = null
    )


    data class Clouds(
            var all: Double? = null
    )


    data class Wind(
            var speed: Double? = null,
            var deg: Double? = null,
            var code: String? = null
    )


    data class Main(
            var temp: Double? = null,
            var humidity: Int? = null,
            var pressure: Int? = null,
            var tempMin: Double? = null,
            var tempMax: Double? = null
    )


    data class Sys(
            var country: String? = null,
            var sunrise: Int? = null,
            var sunset: Int? = null
    )
}