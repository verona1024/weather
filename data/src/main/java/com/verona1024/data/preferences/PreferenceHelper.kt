package com.verona1024.data.preferences

import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.data.preferences.model.LastKnownWeather

interface PreferenceHelper {

    fun getLastKnownWeather(): LastKnownWeather
    fun saveCurrentWeather(lastKnownWeather: WeatherByLocationResponse)
    fun getTemperatureType(): String
    fun getLengthType(): String
}