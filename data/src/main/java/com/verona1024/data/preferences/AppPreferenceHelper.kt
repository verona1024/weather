package com.verona1024.data.preferences

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.content.edit
import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.data.preferences.model.LastKnownWeather
import javax.inject.Inject

class AppPreferenceHelper @Inject constructor(context: Context,
                                              @PreferenceInfo private val prefFileName: String) : PreferenceHelper {
    override fun getTemperatureType(): String = defaultPrefs.getString("settings_temperature", "Kelvin")
    override fun getLengthType(): String = defaultPrefs.getString("settings_length", "Meter")

    override fun saveCurrentWeather(lastKnownWeather: WeatherByLocationResponse) =
            mPrefs.edit {
                putFloat(PREF_KEY_CURRENT_TEMPERATURE, lastKnownWeather.main?.temp!!.toFloat())
                putInt(PREF_KEY_CURRENT_HUMIDITY, lastKnownWeather.main?.humidity!!)
                putInt(PREF_KEY_CURRENT_PRESSURE, lastKnownWeather.main?.pressure!!)

                putFloat(PREF_KEY_CURRENT_WIND_DEG, lastKnownWeather.wind?.deg!!.toFloat())
                putFloat(PREF_KEY_CURRENT_WIND_SPEED, lastKnownWeather.wind?.speed!!.toFloat())
                putFloat(PREF_KEY_CURRENT_RAIN_H, lastKnownWeather.rain?.h!!.toFloat())

                putFloat(PREF_KEY_CURRENT_COOPD_LAT, lastKnownWeather.coord?.lat!!.toFloat())
                putFloat(PREF_KEY_CURRENT_COOPD_LON, lastKnownWeather.coord?.lon!!.toFloat())

                putString(PREF_KEY_CURRENT_CITY, lastKnownWeather.name)

                putString(PREF_KEY_CURRENT_IMG, lastKnownWeather.weather!![0].icon)
                putString(PREF_KEY_CURRENT_DESCRIPTION, lastKnownWeather.weather!![0].description)
            }

    override fun getLastKnownWeather(): LastKnownWeather {
        return LastKnownWeather(
                LastKnownWeather.Coord(
                        mPrefs.getFloat(PREF_KEY_CURRENT_COOPD_LAT, 0f).toDouble(),
                        mPrefs.getFloat(PREF_KEY_CURRENT_COOPD_LON, 0f).toDouble()
                ),
                null,
                LastKnownWeather.Weather(
                        0.0,
                        "",
                        mPrefs.getString(PREF_KEY_CURRENT_DESCRIPTION, ""),
                        mPrefs.getString(PREF_KEY_CURRENT_IMG, "")
                ),
                LastKnownWeather.Main(
                        mPrefs.getFloat(PREF_KEY_CURRENT_TEMPERATURE, 0f).toDouble(),
                        mPrefs.getInt(PREF_KEY_CURRENT_HUMIDITY, 0),
                        mPrefs.getInt(PREF_KEY_CURRENT_PRESSURE, 0),
                        0.0, 0.0
                ),
                LastKnownWeather.Wind(
                        mPrefs.getFloat(PREF_KEY_CURRENT_WIND_SPEED, 0f).toDouble(),
                        mPrefs.getFloat(PREF_KEY_CURRENT_WIND_DEG, 0f).toDouble()
                ),
                LastKnownWeather.Rain(
                        mPrefs.getFloat(PREF_KEY_CURRENT_RAIN_H, 0f).toDouble()
                ),
                null,
                null,
                null,
                mPrefs.getString(PREF_KEY_CURRENT_CITY, "")
        )
    }

    companion object {
        private const val PREF_KEY_CURRENT_CITY = "PREF_KEY_CURRENT_CITY"
        private const val PREF_KEY_CURRENT_TEMPERATURE = "PREF_KEY_CURRENT_TEMPERATURE"
        private const val PREF_KEY_CURRENT_HUMIDITY = "PREF_KEY_CURRENT_HUMIDITY"
        private const val PREF_KEY_CURRENT_PRESSURE = "PREF_KEY_CURRENT_PRESSURE"
        private const val PREF_KEY_CURRENT_WIND_DEG = "PREF_KEY_CURRENT_WIND_DEG"
        private const val PREF_KEY_CURRENT_WIND_SPEED = "PREF_KEY_CURRENT_WIND_SPEED"
        private const val PREF_KEY_CURRENT_COOPD_LAT = "PREF_KEY_CURRENT_COOPD_LAT"
        private const val PREF_KEY_CURRENT_COOPD_LON = "PREF_KEY_CURRENT_COOPD_LON"
        private const val PREF_KEY_CURRENT_IMG = "PREF_KEY_CURRENT_IMG"
        private const val PREF_KEY_CURRENT_DESCRIPTION = "PREF_KEY_CURRENT_DESCRIPTION"
        private const val PREF_KEY_CURRENT_RAIN_H = "PREF_KEY_CURRENT_RAIN_H"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
    private val defaultPrefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
}