package com.verona1024.data.preferences

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class PreferenceInfo