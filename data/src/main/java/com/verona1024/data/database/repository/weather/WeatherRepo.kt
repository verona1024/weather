package com.verona1024.data.database.repository.weather

import io.reactivex.Observable

interface WeatherRepo {

    fun isWeatherRepoEmpty(): Observable<Boolean>
    fun insertWeather(weathers: List<Weather>): Observable<Boolean>
    fun loadWeathers(): Observable<List<Weather>>
    fun deleteWeathers()

}