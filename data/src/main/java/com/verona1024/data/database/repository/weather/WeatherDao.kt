package com.verona1024.data.database.repository.weather

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(weather: List<Weather>)

    @Query("SELECT * FROM weather")
    fun loadAll(): List<Weather>

    @Query("DELETE FROM weather")
    fun deleteAll()
}