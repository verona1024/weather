package com.verona1024.data.database.repository.weather

import io.reactivex.Observable
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val weatherDao: WeatherDao) : WeatherRepo {
    override fun deleteWeathers() = weatherDao.deleteAll()

    override fun isWeatherRepoEmpty(): Observable<Boolean> = Observable.fromCallable({ weatherDao.loadAll().isEmpty() })

    override fun insertWeather(weathers: List<Weather>): Observable<Boolean> {
        weatherDao.insertAll(weathers)
        return Observable.just(true)
    }

    override fun loadWeathers(): Observable<List<Weather>> = Observable.fromCallable({ weatherDao.loadAll() })
}


