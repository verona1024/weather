package com.verona1024.data.database.repository.weather

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "weather")
data class Weather(
        @Expose
        @SerializedName("temp")
        @ColumnInfo(name = "temp")
        var temp: Float,

        @Expose
        @SerializedName("img_url")
        @ColumnInfo(name = "img_url")
        var imgUrl: String?,

        @Expose
        @SerializedName("day")
        @ColumnInfo(name = "day")
        var day: Long?,

        @Expose
        @SerializedName("description")
        @ColumnInfo(name = "description")
        var description: String?
) {
    @Expose
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}