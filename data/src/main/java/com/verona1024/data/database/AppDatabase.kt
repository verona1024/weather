package com.verona1024.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.database.repository.weather.WeatherDao

@Database(entities = [(Weather::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
}