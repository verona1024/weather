package com.verona1024.data.network

import android.location.Location
import com.rx2androidnetworking.Rx2AndroidNetworking
import com.verona1024.data.BuildConfig
import com.verona1024.data.network.models.ForecastResponse
import com.verona1024.data.network.models.WeatherByLocationResponse
import io.reactivex.Observable
import javax.inject.Inject


class AppApiHelper @Inject constructor() : ApiHelper {
    override fun getForecast(lat: Float, lon: Float): Observable<ForecastResponse> =
            Rx2AndroidNetworking
                    .get(ApiEndPoint.ENDPOINT_WEATHER_FORECAST + "?lat=$lat&lon=$lon&cnt=40" + BuildConfig.API_POSTFIX)
                    .build()
                    .getObjectObservable(ForecastResponse::class.java)

    override fun getCurrentLocation(location: Location): Observable<WeatherByLocationResponse> =
            Rx2AndroidNetworking
                    .get(ApiEndPoint.ENDPOINT_WEATHER_BY_LOCATION + "?lat=${location.latitude}&lon=${location.longitude}" + BuildConfig.API_POSTFIX)
                    .build()
                    .getObjectObservable(WeatherByLocationResponse::class.java)

}