package com.verona1024.data.network

import com.verona1024.data.BuildConfig

object ApiEndPoint {

    val ENDPOINT_IMAGE = "http://openweathermap.org/img/w/"
    val ENDPOINT_WEATHER_BY_LOCATION = BuildConfig.BASE_URL + "/data/2.5/weather"
    val ENDPOINT_WEATHER_FORECAST = BuildConfig.BASE_URL + "/data/2.5/forecast"

}