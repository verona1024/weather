package com.verona1024.data.network

import android.location.Location
import com.verona1024.data.network.models.ForecastResponse
import com.verona1024.data.network.models.WeatherByLocationResponse
import io.reactivex.Observable

interface ApiHelper {
    fun getCurrentLocation(location: Location): Observable<WeatherByLocationResponse>
    fun getForecast(lat: Float, lon: Float): Observable<ForecastResponse>
}