package com.verona1024.data.network.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class ForecastResponse(
        @Expose @SerializedName("cod") val cod: String,
        @Expose @SerializedName("message") val message: Double,
        @Expose @SerializedName("cnt") val cnt: Int,
        @Expose @SerializedName("list") val list: List<X>,
        @Expose @SerializedName("city") val city: City
) {

    data class City(
            @Expose @SerializedName("id") val id: Int,
            @Expose @SerializedName("name") val name: String,
            @Expose @SerializedName("coord") val coord: Coord,
            @Expose @SerializedName("country") val country: String,
            @Expose @SerializedName("population") val population: Int
    ) {

        data class Coord(
                @Expose @SerializedName("lat") val lat: Double,
                @Expose @SerializedName("lon") val lon: Double
        )
    }


    data class X(
            @Expose @SerializedName("dt") val dt: Long,
            @Expose @SerializedName("main") val main: Main,
            @Expose @SerializedName("weather") val weather: List<Weather>,
            @Expose @SerializedName("clouds") val clouds: Clouds,
            @Expose @SerializedName("wind") val wind: Wind,
            @Expose @SerializedName("sys") val sys: Sys,
            @Expose @SerializedName("dt_txt") val dtTxt: String
    ) {

        data class Sys(
                @Expose @SerializedName("pod") val pod: String
        )


        data class Main(
                @Expose @SerializedName("temp") val temp: Double,
                @Expose @SerializedName("temp_min") val tempMin: Double,
                @Expose @SerializedName("temp_max") val tempMax: Double,
                @Expose @SerializedName("pressure") val pressure: Double,
                @Expose @SerializedName("sea_level") val seaLevel: Double,
                @Expose @SerializedName("grnd_level") val grndLevel: Double,
                @Expose @SerializedName("humidity") val humidity: Int,
                @Expose @SerializedName("temp_kf") val tempKf: Double
        )


        data class Weather(
                @Expose @SerializedName("id") val id: Long,
                @Expose @SerializedName("main") val main: String,
                @Expose @SerializedName("description") val description: String,
                @Expose @SerializedName("icon") val icon: String
        )


        data class Clouds(
                @Expose @SerializedName("all") val all: Int
        )


        data class Wind(
                @Expose @SerializedName("speed") val speed: Double,
                @Expose @SerializedName("deg") val deg: Double
        )
    }
}