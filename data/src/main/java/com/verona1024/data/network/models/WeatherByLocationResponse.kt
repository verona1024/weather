package com.verona1024.data.network.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class WeatherByLocationResponse(
        @Expose @SerializedName("coord") var coord: Coord? = null,
        @Expose @SerializedName("sys") var sys: Sys? = null,
        @Expose @SerializedName("weather") var weather: List<Weather>? = null,
        @Expose @SerializedName("main") var main: Main? = null,
        @Expose @SerializedName("wind") var wind: Wind? = null,
        @Expose @SerializedName("rain") var rain: Rain? = null,
        @Expose @SerializedName("clouds") var clouds: Clouds? = null,
        @Expose @SerializedName("dt") var dt: Double? = null,
        @Expose @SerializedName("id") var id: Double? = null,
        @Expose @SerializedName("name") var name: String? = null,
        @Expose @SerializedName("cod") var cod: Double? = null
) {

    data class Coord(
            @Expose @SerializedName("lon") var lon: Double? = null,
            @Expose @SerializedName("lat") var lat: Double? = null
    )


    data class Rain(
            @Expose @SerializedName("3h") var h: Double? = null
    )


    data class Weather(
            @Expose @SerializedName("id") var id: Double? = null,
            @Expose @SerializedName("main") var main: String? = null,
            @Expose @SerializedName("description") var description: String? = null,
            @Expose @SerializedName("icon") var icon: String? = null
    )


    data class Clouds(
            @Expose @SerializedName("all") var all: Double? = null
    )


    data class Wind(
            @Expose @SerializedName("speed") var speed: Double? = null,
            @Expose @SerializedName("deg") var deg: Double? = null,
            @Expose @SerializedName("code") var code: String? = null
    )


    data class Main(
            @Expose @SerializedName("temp") var temp: Double? = null,
            @Expose @SerializedName("humidity") var humidity: Int? = null,
            @Expose @SerializedName("pressure") var pressure: Int? = null,
            @Expose @SerializedName("temp_min") var tempMin: Double? = null,
            @Expose @SerializedName("temp_max") var tempMax: Double? = null
    )


    data class Sys(
            @Expose @SerializedName("country") var country: String? = null,
            @Expose @SerializedName("sunrise") var sunrise: Int? = null,
            @Expose @SerializedName("sunset") var sunset: Int? = null
    )
}