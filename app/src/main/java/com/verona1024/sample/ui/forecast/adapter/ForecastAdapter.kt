package com.verona1024.sample.ui.forecast.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.network.ApiEndPoint
import com.verona1024.sample.BuildConfig
import com.verona1024.sample.R
import com.verona1024.sample.util.FormattedString
import com.verona1024.sample.util.extension.loadImage
import kotlinx.android.synthetic.main.item_forecast.view.*
import java.util.*

class ForecastAdapter(private val items: ArrayList<Weather>, val context: Context) : RecyclerView.Adapter<WeatherViewHolder>() {
    private lateinit var tempType: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(LayoutInflater.from(context).inflate(R.layout.item_forecast, parent, false))
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val weatherItem = items[position]

        holder.tvState.text = FormattedString.forecastFormat(weatherItem.description!!, weatherItem.day!!)
        holder.tvTemperature.text = FormattedString.temperatureFormat(weatherItem.temp.toDouble(), holder.tvTemperature.context, tempType)
        holder.weather_image.loadImage(ApiEndPoint.ENDPOINT_IMAGE + weatherItem.imgUrl + ".png")
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(list: List<Weather>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun setTempType(tempType: String): ForecastAdapter {
        this.tempType = tempType
        return this
    }
}

class WeatherViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvState = view.text_state
    val tvTemperature = view.text_temperature
    val weather_image = view.weather_image
}