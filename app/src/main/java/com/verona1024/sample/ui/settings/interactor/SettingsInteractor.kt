package com.verona1024.sample.ui.settings.interactor

import android.location.Location
import com.verona1024.sample.ui.base.interactor.BaseInteractor
import com.verona1024.data.network.ApiHelper
import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.data.preferences.PreferenceHelper
import io.reactivex.Observable
import javax.inject.Inject

class SettingsInteractor @Inject internal constructor(apiHelper: ApiHelper, preferenceHelper: PreferenceHelper) : BaseInteractor(apiHelper, preferenceHelper), SettingsMVPInteractor {
    override fun saveWeather(weatherByLocationResponse: WeatherByLocationResponse) = preferenceHelper.saveCurrentWeather(weatherByLocationResponse)
    override fun getWeatherApiCall(location: Location): Observable<WeatherByLocationResponse> = apiHelper.getCurrentLocation(location)
}


