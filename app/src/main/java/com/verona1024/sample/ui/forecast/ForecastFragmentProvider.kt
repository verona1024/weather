package com.verona1024.sample.ui.forecast

import com.verona1024.sample.ui.forecast.view.ForecastFragment
import com.verona1024.sample.ui.today.view.TodayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ForecastFragmentProvider {

    @ContributesAndroidInjector(modules = [ForecastFragmentModule::class])
    internal abstract fun provideForecastFragment(): ForecastFragment
}