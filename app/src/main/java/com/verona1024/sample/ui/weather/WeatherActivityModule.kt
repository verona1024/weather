package com.verona1024.sample.ui.weather

import com.verona1024.sample.ui.weather.interactor.WeatherInteractor
import com.verona1024.sample.ui.weather.interactor.WeatherMVPInteractor
import com.verona1024.sample.ui.weather.presenter.WeatherMVPPresenter
import com.verona1024.sample.ui.weather.presenter.WeatherPresenter
import com.verona1024.sample.ui.weather.view.WeatherMVPView
import dagger.Module
import dagger.Provides

@Module
class WeatherActivityModule {

    @Provides
    internal fun provideWeatherInteractor(interactor: WeatherInteractor): WeatherMVPInteractor = interactor

    @Provides
    internal fun provideWeatherPresenter(presenter: WeatherPresenter<WeatherMVPView, WeatherMVPInteractor>)
            : WeatherMVPPresenter<WeatherMVPView, WeatherMVPInteractor> = presenter

}