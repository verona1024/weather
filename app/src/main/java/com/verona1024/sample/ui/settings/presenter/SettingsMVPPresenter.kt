package com.verona1024.sample.ui.settings.presenter

import com.verona1024.sample.ui.base.presenter.MVPPresenter
import com.verona1024.sample.ui.settings.interactor.SettingsMVPInteractor
import com.verona1024.sample.ui.settings.view.SettingsMVPView

interface SettingsMVPPresenter<V : SettingsMVPView, I : SettingsMVPInteractor> : MVPPresenter<V, I>