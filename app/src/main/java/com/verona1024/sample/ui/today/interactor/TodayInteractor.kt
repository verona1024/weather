package com.verona1024.sample.ui.today.interactor

import com.verona1024.data.preferences.PreferenceHelper
import com.verona1024.sample.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class TodayInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper) : BaseInteractor(preferenceHelper), TodayMVPInteractor {
    override fun getLastKnownWeather() = preferenceHelper.getLastKnownWeather()
    override fun getTemperatureType() = preferenceHelper.getTemperatureType()
    override fun getLenghtType() = preferenceHelper.getLengthType()
}


