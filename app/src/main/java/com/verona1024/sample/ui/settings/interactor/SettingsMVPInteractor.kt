package com.verona1024.sample.ui.settings.interactor

import android.location.Location
import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.sample.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface SettingsMVPInteractor : MVPInteractor {

    fun getWeatherApiCall(location: Location): Observable<WeatherByLocationResponse>
    fun saveWeather(weatherByLocationResponse: WeatherByLocationResponse)
}