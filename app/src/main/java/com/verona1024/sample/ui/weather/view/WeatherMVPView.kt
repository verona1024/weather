package com.verona1024.sample.ui.weather.view

import com.verona1024.sample.ui.base.view.MVPView

interface WeatherMVPView : MVPView {
    fun openSettings()
    fun showAbout()
    fun openToday()
    fun openForecast()
}