package com.verona1024.sample.ui.splash

import com.verona1024.sample.ui.splash.interactor.SplashInteractor
import com.verona1024.sample.ui.splash.interactor.SplashMVPInteractor
import com.verona1024.sample.ui.splash.presenter.SplashMVPPresenter
import com.verona1024.sample.ui.splash.presenter.SplashPresenter
import com.verona1024.sample.ui.splash.view.SplashMVPView
import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashInteractor(interactor: SplashInteractor): SplashMVPInteractor = interactor

    @Provides
    internal fun provideSplashPresenter(presenter: SplashPresenter<SplashMVPView, SplashMVPInteractor>)
            : SplashMVPPresenter<SplashMVPView, SplashMVPInteractor> = presenter

}