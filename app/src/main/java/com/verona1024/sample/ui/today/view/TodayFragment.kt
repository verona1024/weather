package com.verona1024.sample.ui.today.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.verona1024.data.network.ApiEndPoint
import com.verona1024.sample.R
import com.verona1024.sample.ui.base.view.BaseFragment
import com.verona1024.sample.ui.today.interactor.TodayMVPInteractor
import com.verona1024.sample.ui.today.presenter.TodayMVPPresenter
import com.verona1024.sample.util.extension.loadImage
import kotlinx.android.synthetic.main.fragment_today.*
import javax.inject.Inject

class TodayFragment : BaseFragment(), TodayMVPView {
    override fun setUp() {
    }

    companion object {
        fun newInstance(): TodayFragment {
            return TodayFragment()
        }

        val TAG = TodayFragment::class.java.simpleName
    }

    @Inject
    internal lateinit var presenter: TodayMVPPresenter<TodayMVPView, TodayMVPInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_today, container, false)

    override fun onResume() {
        super.onResume()
        presenter.onAttach(this)
        presenter.refreshWeatherInfo()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun setWeatherInfo(temperature: String, location: String, direction: String, humidity: String, percipitation: String, pressure: String, wind: String, imgUrl: String, description: String) {
        text_temperature.text = temperature
        text_direction.text = direction
        text_humidity.text = humidity
        text_location.text = location
        text_precipitation.text = percipitation
        text_pressure.text = pressure
        text_wind.text = wind
        text_description.text = description.capitalize()
        image_weather.loadImage(ApiEndPoint.ENDPOINT_IMAGE + imgUrl + ".png")
    }

    override fun getViewContext(): Context {
        return context!!
    }

}
