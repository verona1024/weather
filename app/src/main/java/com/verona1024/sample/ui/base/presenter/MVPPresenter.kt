package com.verona1024.sample.ui.base.presenter

import com.verona1024.sample.ui.base.interactor.MVPInteractor
import com.verona1024.sample.ui.base.view.MVPView

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}