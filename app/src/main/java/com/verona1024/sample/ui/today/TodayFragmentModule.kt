package com.verona1024.sample.ui.today

import com.verona1024.sample.ui.today.interactor.TodayInteractor
import com.verona1024.sample.ui.today.interactor.TodayMVPInteractor
import com.verona1024.sample.ui.today.presenter.TodayMVPPresenter
import com.verona1024.sample.ui.today.presenter.TodayPresenter
import com.verona1024.sample.ui.today.view.TodayMVPView
import dagger.Module
import dagger.Provides

@Module
class TodayFragmentModule {

    @Provides
    internal fun provideTodayInteractor(interactor: TodayInteractor): TodayMVPInteractor = interactor

    @Provides
    internal fun provideTodayPresenter(presenter: TodayPresenter<TodayMVPView, TodayMVPInteractor>)
            : TodayMVPPresenter<TodayMVPView, TodayMVPInteractor> = presenter

}