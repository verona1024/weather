package com.verona1024.sample.ui.base.view


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

abstract class BaseActivity : AppCompatActivity(), MVPView, BaseFragment.CallBack {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDI()
    }

    override fun onResume() {
        super.onResume()
        performDI()
    }

    private fun performDI() = AndroidInjection.inject(this)

}