package com.verona1024.sample.ui.weather.presenter

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.network.models.ForecastResponse
import com.verona1024.sample.ui.base.presenter.BasePresenter
import com.verona1024.sample.ui.weather.interactor.WeatherMVPInteractor
import com.verona1024.sample.ui.weather.view.WeatherMVPView
import com.verona1024.sample.util.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class WeatherPresenter<V : WeatherMVPView, I : WeatherMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), WeatherMVPPresenter<V, I> {
    override fun getForecast() {
        val lastWeather = interactor?.getLastKnownWeather()
        interactor?.let {
            compositeDisposable.add(it.getForecastApiCall(lastWeather?.coord?.lat?.toFloat()!!, lastWeather.coord?.lon?.toFloat()!!)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ forecastResponse ->
                        saveWeather(forecastResponse)
                    }, { err -> err.printStackTrace() })
            )
        }
    }

    override fun onSettingsClick() {
        getView()?.openSettings()
    }

    override fun onAboutClick() {
        getView()?.showAbout()
    }

    override fun onDrawerTodayClick() {
        getView()?.openToday()
    }

    override fun onDrawerForecastClick() {
        getView()?.openForecast()
    }

    private fun saveWeather(forecastResponse: ForecastResponse) {
        interactor?.let {
            compositeDisposable.add(
                    Observable.just(forecastResponse.list)
                            .compose(schedulerProvider.ioToioObservableScheduler())
                            .flatMapIterable { x -> x }
                            .map { item -> Weather(item.main.tempMax.toFloat(), item.weather[0].icon, item.dt, item.weather[0].description) }
                            .toList()
                            .subscribe({ weathers -> it.saveForecast(weathers) }, { err -> err.printStackTrace() })
            )
        }
    }
}