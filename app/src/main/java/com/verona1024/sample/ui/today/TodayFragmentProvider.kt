package com.verona1024.sample.ui.today

import com.verona1024.sample.ui.today.view.TodayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class TodayFragmentProvider {

    @ContributesAndroidInjector(modules = [TodayFragmentModule::class])
    internal abstract fun provideTodayFragment(): TodayFragment
}