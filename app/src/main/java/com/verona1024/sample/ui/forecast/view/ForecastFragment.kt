package com.verona1024.sample.ui.forecast.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.sample.R
import com.verona1024.sample.ui.base.view.BaseFragment
import com.verona1024.sample.ui.forecast.adapter.ForecastAdapter
import com.verona1024.sample.ui.forecast.interactor.ForecastMVPInteractor
import com.verona1024.sample.ui.forecast.presenter.ForecastMVPPresenter
import kotlinx.android.synthetic.main.fragment_forecast.*
import javax.inject.Inject

class ForecastFragment : BaseFragment(), ForecastMVPView {
    override fun setUp() {
    }

    companion object {
        fun newInstance(): ForecastFragment {
            return ForecastFragment()
        }

        val TAG = ForecastFragment::class.java.simpleName
    }

    private val weatherItems: List<Weather> = ArrayList()

    @Inject
    internal lateinit var presenter: ForecastMVPPresenter<ForecastMVPView, ForecastMVPInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_forecast, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        forecast_recycler.layoutManager = LinearLayoutManager(getBaseActivity())
        forecast_recycler.adapter = ForecastAdapter(weatherItems as ArrayList<Weather>, getBaseActivity()!!.applicationContext)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach(this)
        presenter.refreshForecastList()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun refreshForecastList(forecast: List<Weather>, tempType: String) {
        (forecast_recycler.adapter as ForecastAdapter)
                .setTempType(tempType)
                .setItems(forecast)
    }

}
