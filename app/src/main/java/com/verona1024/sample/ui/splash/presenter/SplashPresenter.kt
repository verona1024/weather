package com.verona1024.sample.ui.splash.presenter

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.support.v4.content.ContextCompat
import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.sample.ui.base.presenter.BasePresenter
import com.verona1024.sample.ui.splash.interactor.SplashMVPInteractor
import com.verona1024.sample.ui.splash.view.SplashMVPView
import com.verona1024.sample.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenter<V : SplashMVPView, I : SplashMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), SplashMVPPresenter<V, I> {
    override fun getWeatherByLocation(location: Location) {
        interactor?.let {
            compositeDisposable.add(it.getWeatherApiCall(location)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ weatherByLocationResponse ->
                        saveWeather(weatherByLocationResponse)
                        getView()?.launchNextActivity()
                    }, { err ->
                        print(err)
                        getView()?.launchNextActivity()
                    })
            )
        }
    }

    private fun saveWeather(weatherByLocationResponse: WeatherByLocationResponse) = interactor?.saveWeather(weatherByLocationResponse)

    override fun checkPermission(context: Context) {
        val permissionCOARSE = ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        val permissionFine = ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)

        if (permissionCOARSE != PackageManager.PERMISSION_GRANTED
                || permissionFine != PackageManager.PERMISSION_GRANTED) {
            getView()?.requestPermission(Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION)
        } else {
            getView()?.getLocation()
        }
    }


}