package com.verona1024.sample.ui.settings.presenter

import com.verona1024.sample.ui.base.presenter.BasePresenter
import com.verona1024.sample.ui.settings.interactor.SettingsMVPInteractor
import com.verona1024.sample.ui.settings.view.SettingsMVPView
import com.verona1024.sample.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SettingsPresenter<V : SettingsMVPView, I : SettingsMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), SettingsMVPPresenter<V, I>