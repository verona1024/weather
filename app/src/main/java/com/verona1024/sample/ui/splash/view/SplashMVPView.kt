package com.verona1024.sample.ui.splash.view

import com.verona1024.sample.ui.base.view.MVPView

interface SplashMVPView : MVPView {
    fun requestPermission(vararg permissions: String)
    fun getLocation()
    fun launchNextActivity()
}