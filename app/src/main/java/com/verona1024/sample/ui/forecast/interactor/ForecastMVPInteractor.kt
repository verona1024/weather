package com.verona1024.sample.ui.forecast.interactor

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.sample.ui.base.interactor.MVPInteractor
import io.reactivex.Observable
import io.reactivex.Single

interface ForecastMVPInteractor : MVPInteractor {
    fun getWeathersList(): Observable<List<Weather>>
    fun getTemperatureType(): String
}