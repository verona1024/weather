package com.verona1024.sample.ui.forecast.presenter

import com.verona1024.sample.ui.base.presenter.MVPPresenter
import com.verona1024.sample.ui.forecast.interactor.ForecastMVPInteractor
import com.verona1024.sample.ui.forecast.view.ForecastMVPView

interface ForecastMVPPresenter<V : ForecastMVPView, I : ForecastMVPInteractor> : MVPPresenter<V, I> {
    fun refreshForecastList()
}