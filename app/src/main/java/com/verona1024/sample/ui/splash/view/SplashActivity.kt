package com.verona1024.sample.ui.splash.view

import android.annotation.SuppressLint
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import com.verona1024.sample.R
import com.verona1024.sample.ui.base.view.BaseActivity
import com.verona1024.sample.ui.splash.interactor.SplashMVPInteractor
import com.verona1024.sample.ui.splash.presenter.SplashMVPPresenter
import com.verona1024.sample.ui.weather.view.WeatherActivity
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashMVPView {
    override fun launchNextActivity() {
        startActivity(WeatherActivity.newIntent(this))
        finish()
    }

    @SuppressLint("MissingPermission")
    override fun getLocation() {
        // Todo: update location catch
        val location = locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        if (location != null) {
            presenter.getWeatherByLocation(location)
        }
    }

    private val RECORD_REQUEST_CODE = 101

    override fun onFragmentDetached(tag: String) {
    }

    override fun onFragmentAttached() {
    }

    private var locationManager: LocationManager? = null

    @Inject
    internal lateinit var presenter: SplashMVPPresenter<SplashMVPView, SplashMVPInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
    }

    override fun requestPermission(vararg permissions: String) {
        ActivityCompat.requestPermissions(this,
                permissions,
                RECORD_REQUEST_CODE)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach(this)
        presenter.checkPermission(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {
                presenter.checkPermission(this)
            }
        }
    }
}
