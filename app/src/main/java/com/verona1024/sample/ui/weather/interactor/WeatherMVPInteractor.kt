package com.verona1024.sample.ui.weather.interactor

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.network.models.ForecastResponse
import com.verona1024.data.preferences.model.LastKnownWeather
import com.verona1024.sample.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface WeatherMVPInteractor : MVPInteractor {
    fun getLastKnownWeather(): LastKnownWeather
    fun getForecastApiCall(lat: Float, lon: Float): Observable<ForecastResponse>
    fun saveForecast(weathers: List<Weather>)
}