package com.verona1024.sample.ui.base.interactor

import com.verona1024.data.database.repository.weather.WeatherRepo
import com.verona1024.data.network.ApiHelper
import com.verona1024.data.preferences.PreferenceHelper

open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper
    protected lateinit var database: WeatherRepo

    constructor(apiHelper: ApiHelper, preferenceHelper: PreferenceHelper) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
    }

    constructor(preferenceHelper: PreferenceHelper) : this() {
        this.preferenceHelper = preferenceHelper
    }

    constructor(apiHelper: ApiHelper, database: WeatherRepo, preferenceHelper: PreferenceHelper) : this() {
        this.database = database
        this.apiHelper = apiHelper
        this.preferenceHelper = preferenceHelper
    }

    constructor(database: WeatherRepo, preferenceHelper: PreferenceHelper) : this() {
        this.database = database
        this.preferenceHelper = preferenceHelper
    }
}