package com.verona1024.sample.ui.forecast.interactor

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.database.repository.weather.WeatherRepo
import com.verona1024.data.preferences.PreferenceHelper
import com.verona1024.sample.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ForecastInteractor @Inject internal constructor(database: WeatherRepo, preferenceHelper: PreferenceHelper) : BaseInteractor(database, preferenceHelper), ForecastMVPInteractor {
    override fun getTemperatureType() = preferenceHelper.getTemperatureType()
    override fun getWeathersList(): Observable<List<Weather>> = database.loadWeathers()
}


