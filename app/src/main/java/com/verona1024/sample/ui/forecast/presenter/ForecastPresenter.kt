package com.verona1024.sample.ui.forecast.presenter

import com.verona1024.sample.ui.base.presenter.BasePresenter
import com.verona1024.sample.ui.forecast.interactor.ForecastMVPInteractor
import com.verona1024.sample.ui.forecast.view.ForecastMVPView
import com.verona1024.sample.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ForecastPresenter<V : ForecastMVPView, I : ForecastMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), ForecastMVPPresenter<V, I> {
    override fun refreshForecastList() {
        interactor?.let {
            it.getWeathersList()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ list -> getView()?.refreshForecastList(list, interactor?.getTemperatureType()!!) }, { err -> })
        }
    }
}