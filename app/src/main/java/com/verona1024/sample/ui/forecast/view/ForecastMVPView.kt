package com.verona1024.sample.ui.forecast.view

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.sample.ui.base.view.MVPView

interface ForecastMVPView : MVPView {
    fun refreshForecastList(forecast: List<Weather>, tempType: String)
}