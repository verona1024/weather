package com.verona1024.sample.ui.today.view

import android.content.Context
import com.verona1024.sample.ui.base.view.MVPView

interface TodayMVPView : MVPView {
    fun setWeatherInfo(temperature: String, location: String, direction: String, humidity: String, percipitation: String, pressure: String, wind: String, imgUrl: String, description: String)
    fun getViewContext(): Context
}