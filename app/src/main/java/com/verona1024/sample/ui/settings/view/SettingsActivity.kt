package com.verona1024.sample.ui.settings.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.*
import com.verona1024.sample.R
import com.verona1024.sample.ui.base.view.BaseActivity
import com.verona1024.sample.ui.settings.interactor.SettingsMVPInteractor
import com.verona1024.sample.ui.settings.presenter.SettingsMVPPresenter
import com.verona1024.sample.util.extension.addFragment
import javax.inject.Inject

class SettingsActivity : BaseActivity(), SettingsMVPView {
    companion object {
        fun newIntent(context: Context) = Intent(context, SettingsActivity::class.java)

        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->

            val index = (preference as ListPreference).findIndexOfValue(value.toString())

            preference.setSummary(
                    if (index >= 0) {
                        preference.entries[index]
                    } else {
                        null
                    })
            true
        }

        private fun bindPreferenceSummaryToValue(preference: Preference) {
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun onFragmentAttached() {
    }

    @Inject
    internal lateinit var presenter: SettingsMVPPresenter<SettingsMVPView, SettingsMVPInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        presenter.onAttach(this)
        fragmentManager.addFragment(android.R.id.content, SettingsFragment.newInstance(), SettingsFragment.TAG)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class SettingsFragment : PreferenceFragment() {
        companion object {
            fun newInstance() = SettingsFragment()
            val TAG = SettingsFragment::class.java.simpleName
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences)
            bindPreferenceSummaryToValue(findPreference("settings_length"))
            bindPreferenceSummaryToValue(findPreference("settings_temperature"))
        }
    }
}
