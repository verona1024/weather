package com.verona1024.sample.ui.today.presenter

import com.verona1024.sample.ui.base.presenter.BasePresenter
import com.verona1024.sample.ui.today.interactor.TodayMVPInteractor
import com.verona1024.sample.ui.today.view.TodayMVPView
import com.verona1024.sample.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import com.verona1024.sample.util.FormattedString
import javax.inject.Inject

class TodayPresenter<V : TodayMVPView, I : TodayMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), TodayMVPPresenter<V, I> {
    override fun refreshWeatherInfo() {
        val lastWeather = interactor?.getLastKnownWeather()
        getView()?.setWeatherInfo(
                FormattedString.temperatureFormat(lastWeather?.main?.temp!!, getView()?.getViewContext()!!, interactor?.getTemperatureType()!!),
                lastWeather.name!!,
                lastWeather.wind?.deg.toString(),
                "${lastWeather.main?.humidity.toString()}%",
                FormattedString.smallLenghtFormat(lastWeather.rain?.h!!, getView()?.getViewContext()!!, interactor?.getLenghtType()!!),
                "${lastWeather.main?.pressure.toString()} hPa",
                FormattedString.speedFormat(lastWeather.wind?.speed!!, getView()?.getViewContext()!!, interactor?.getLenghtType()!!),
                lastWeather.weather?.icon!!,
                lastWeather.weather?.description!!
        )
    }
}