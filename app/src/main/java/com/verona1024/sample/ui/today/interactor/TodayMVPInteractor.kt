package com.verona1024.sample.ui.today.interactor

import com.verona1024.data.preferences.model.LastKnownWeather
import com.verona1024.sample.ui.base.interactor.MVPInteractor

interface TodayMVPInteractor : MVPInteractor {
    fun getLastKnownWeather(): LastKnownWeather
    fun getTemperatureType(): String
    fun getLenghtType(): String
}