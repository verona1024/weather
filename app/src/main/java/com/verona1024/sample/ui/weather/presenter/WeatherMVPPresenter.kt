package com.verona1024.sample.ui.weather.presenter

import com.verona1024.sample.ui.base.presenter.MVPPresenter
import com.verona1024.sample.ui.weather.interactor.WeatherMVPInteractor
import com.verona1024.sample.ui.weather.view.WeatherMVPView

interface WeatherMVPPresenter<V : WeatherMVPView, I : WeatherMVPInteractor> : MVPPresenter<V, I> {

    fun onSettingsClick()
    fun onAboutClick()
    fun onDrawerTodayClick()
    fun onDrawerForecastClick()

    fun getForecast()
}