package com.verona1024.sample.ui.weather.interactor

import com.verona1024.data.database.repository.weather.Weather
import com.verona1024.data.database.repository.weather.WeatherRepo
import com.verona1024.data.network.ApiHelper
import com.verona1024.data.preferences.PreferenceHelper
import com.verona1024.sample.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class WeatherInteractor @Inject internal constructor(apiHelper: ApiHelper, database: WeatherRepo, preferenceHelper: PreferenceHelper) : BaseInteractor(apiHelper, database, preferenceHelper), WeatherMVPInteractor {
    override fun getForecastApiCall(lat: Float, lon: Float) = apiHelper.getForecast(lat, lon)

    override fun saveForecast(weathers: List<Weather>) {
        database.deleteWeathers()
        database.insertWeather(weathers)
    }

    override fun getLastKnownWeather() = preferenceHelper.getLastKnownWeather()

}


