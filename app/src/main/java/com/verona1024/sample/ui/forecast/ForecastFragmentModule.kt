package com.verona1024.sample.ui.forecast

import com.verona1024.sample.ui.forecast.interactor.ForecastInteractor
import com.verona1024.sample.ui.forecast.interactor.ForecastMVPInteractor
import com.verona1024.sample.ui.forecast.presenter.ForecastMVPPresenter
import com.verona1024.sample.ui.forecast.presenter.ForecastPresenter
import com.verona1024.sample.ui.forecast.view.ForecastMVPView
import dagger.Module
import dagger.Provides

@Module
class ForecastFragmentModule {

    @Provides
    internal fun provideForecastInteractor(interactor: ForecastInteractor): ForecastMVPInteractor = interactor

    @Provides
    internal fun provideTodayPresenter(presenter: ForecastPresenter<ForecastMVPView, ForecastMVPInteractor>)
            : ForecastMVPPresenter<ForecastMVPView, ForecastMVPInteractor> = presenter

}