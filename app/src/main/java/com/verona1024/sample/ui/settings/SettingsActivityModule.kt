package com.verona1024.sample.ui.settings

import com.verona1024.sample.ui.settings.interactor.SettingsInteractor
import com.verona1024.sample.ui.settings.interactor.SettingsMVPInteractor
import com.verona1024.sample.ui.settings.presenter.SettingsMVPPresenter
import com.verona1024.sample.ui.settings.presenter.SettingsPresenter
import com.verona1024.sample.ui.settings.view.SettingsMVPView
import dagger.Module
import dagger.Provides

@Module
class SettingsActivityModule {

    @Provides
    internal fun provideSettingsInteractor(interactor: SettingsInteractor): SettingsMVPInteractor = interactor

    @Provides
    internal fun provideSettingsPresenter(presenter: SettingsPresenter<SettingsMVPView, SettingsMVPInteractor>)
            : SettingsMVPPresenter<SettingsMVPView, SettingsMVPInteractor> = presenter

}