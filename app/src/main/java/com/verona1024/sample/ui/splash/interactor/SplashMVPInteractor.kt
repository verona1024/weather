package com.verona1024.sample.ui.splash.interactor

import android.location.Location
import com.verona1024.data.network.models.WeatherByLocationResponse
import com.verona1024.sample.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface SplashMVPInteractor : MVPInteractor {

    fun getWeatherApiCall(location: Location): Observable<WeatherByLocationResponse>
    fun saveWeather(weatherByLocationResponse: WeatherByLocationResponse)
}