package com.verona1024.sample.ui.weather.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.verona1024.sample.R
import com.verona1024.sample.ui.base.view.BaseActivity
import com.verona1024.sample.ui.forecast.view.ForecastFragment
import com.verona1024.sample.ui.today.view.TodayFragment
import com.verona1024.sample.ui.weather.interactor.WeatherMVPInteractor
import com.verona1024.sample.ui.settings.view.SettingsActivity
import com.verona1024.sample.ui.weather.presenter.WeatherMVPPresenter
import com.verona1024.sample.util.extension.addFragment
import com.verona1024.sample.util.extension.removeFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class WeatherActivity : BaseActivity(), WeatherMVPView, NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector, DrawerLayout.DrawerListener {
    companion object {
        fun newIntent(context: Context) = Intent(context, WeatherActivity::class.java)
    }

    override fun onDrawerStateChanged(newState: Int) {
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
    }

    override fun onDrawerClosed(drawerView: View) {
    }

    override fun onDrawerOpened(drawerView: View) {
        setTitle(R.string.app_name)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun openSettings() {
        startActivity(SettingsActivity.newIntent(this))
    }

    override fun showAbout() {
        AlertDialog.Builder(this@WeatherActivity)
                .setTitle(getString(R.string.about_dialog_title))
                .setMessage(getString(R.string.about_dialog_message))
                .setPositiveButton(getString(R.string.about_dialog_positive_button), { dialogInterface, i -> })
                .create().show()
    }

    override fun openToday() {
        supportFragmentManager.removeFragment(ForecastFragment.TAG)
        supportFragmentManager.addFragment(R.id.content, TodayFragment.newInstance(), TodayFragment.TAG)
    }

    override fun openForecast() {
        supportFragmentManager.removeFragment(TodayFragment.TAG)
        supportFragmentManager.addFragment(R.id.content, ForecastFragment.newInstance(), ForecastFragment.TAG)
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun onFragmentAttached() {
    }

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    internal lateinit var presenter: WeatherMVPPresenter<WeatherMVPView, WeatherMVPInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        drawer_layout.addDrawerListener(this)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        presenter.onDrawerTodayClick()
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach(this)
        presenter.getForecast()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                presenter.onSettingsClick()
                true
            }
            R.id.action_about -> {
                presenter.onAboutClick()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_today -> {
                presenter.onDrawerTodayClick()
                setTitle(R.string.main_drawer_today)
            }
            R.id.nav_forecast -> {
                presenter.onDrawerForecastClick()
                setTitle(R.string.main_drawer_forecast)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
