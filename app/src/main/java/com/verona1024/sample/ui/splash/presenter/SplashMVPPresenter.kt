package com.verona1024.sample.ui.splash.presenter

import android.content.Context
import android.location.Location
import com.verona1024.sample.ui.base.presenter.MVPPresenter
import com.verona1024.sample.ui.splash.interactor.SplashMVPInteractor
import com.verona1024.sample.ui.splash.view.SplashMVPView

interface SplashMVPPresenter<V : SplashMVPView, I : SplashMVPInteractor> : MVPPresenter<V, I> {

    fun checkPermission(context: Context)
    fun getWeatherByLocation(location: Location)
}