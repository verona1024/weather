package com.verona1024.sample.ui.today.presenter

import com.verona1024.sample.ui.base.presenter.MVPPresenter
import com.verona1024.sample.ui.today.interactor.TodayMVPInteractor
import com.verona1024.sample.ui.today.view.TodayMVPView

interface TodayMVPPresenter<V : TodayMVPView, I : TodayMVPInteractor> : MVPPresenter<V, I> {
    fun refreshWeatherInfo()
}