package com.verona1024.sample.di.builder

import com.verona1024.sample.ui.forecast.ForecastFragmentProvider
import com.verona1024.sample.ui.settings.SettingsActivityModule
import com.verona1024.sample.ui.settings.view.SettingsActivity
import com.verona1024.sample.ui.splash.SplashActivityModule
import com.verona1024.sample.ui.splash.view.SplashActivity
import com.verona1024.sample.ui.today.TodayFragmentProvider
import com.verona1024.sample.ui.weather.WeatherActivityModule
import com.verona1024.sample.ui.weather.view.WeatherActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(WeatherActivityModule::class), (TodayFragmentProvider::class), (ForecastFragmentProvider::class)])
    abstract fun bindWeatherActivity(): WeatherActivity

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [(SettingsActivityModule::class)])
    abstract fun bindSettingsActivity(): SettingsActivity
}