package com.verona1024.sample.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.verona1024.data.database.AppDatabase
import com.verona1024.data.database.repository.weather.WeatherRepo
import com.verona1024.data.database.repository.weather.WeatherRepository
import com.verona1024.data.network.ApiHelper
import com.verona1024.data.network.AppApiHelper
import com.verona1024.data.preferences.AppPreferenceHelper
import com.verona1024.data.preferences.PreferenceHelper
import com.verona1024.data.preferences.PreferenceInfo
import com.verona1024.sample.util.AppConstants
import com.verona1024.sample.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConstants.PREF_NAME

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideQuestionRepoHelper(appDatabase: AppDatabase): WeatherRepo = WeatherRepository(appDatabase.weatherDao())


    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()


}