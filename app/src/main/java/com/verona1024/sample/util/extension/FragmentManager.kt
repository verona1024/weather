package com.verona1024.sample.util.extension

internal fun android.support.v4.app.FragmentManager.removeFragment(tag: String) {
    if (this.findFragmentByTag(tag) != null) {
        this.beginTransaction()
                .disallowAddToBackStack()
                .remove(this.findFragmentByTag(tag))
                .commitNow()
    }
}

internal fun android.support.v4.app.FragmentManager.addFragment(containerViewId: Int,
                                                                fragment: android.support.v4.app.Fragment,
                                                                tag: String) {
    this.beginTransaction()
            .disallowAddToBackStack()
            .add(containerViewId, fragment, tag)
            .commit()
}

internal fun android.app.FragmentManager.addFragment(containerViewId: Int,
                                                     fragment: android.app.Fragment,
                                                     tag: String) {
    this.beginTransaction()
            .disallowAddToBackStack()
            .add(containerViewId, fragment, tag)
            .commit()
}

