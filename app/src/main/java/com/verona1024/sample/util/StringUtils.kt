package com.verona1024.sample.util

import android.content.Context
import com.verona1024.sample.R
import java.text.SimpleDateFormat
import java.util.*

object FormattedString {
    fun temperatureFormat(temperature: Double, context: Context, type: String) =
            String.format("${temperatureValue(context, type, temperature)}${temperatureSymbol(type, context)}", temperature.toLong())

    private fun temperatureSymbol(type: String, context: Context): String {
        val temperatureArray = context.resources.getStringArray(R.array.settings_temperature)
        val symbolsArray = context.resources.getStringArray(R.array.temperature_symbol)

        return symbolsArray[temperatureArray.indexOf(type)]
    }

    private fun temperatureValue(context: Context, type: String, temperatue: Double) =
            when (context.resources.getStringArray(R.array.settings_temperature).indexOf(type)) {
                0 -> {
                    (temperatue - 273.15F).toInt()
                }
                1 -> {
                    ((((temperatue - 273F) * 9F / 5F) + 32F)).toInt()
                }
                2 -> {
                    (temperatue).toInt()
                }
                else -> {
                    (temperatue).toInt()
                }
            }

    fun forecastFormat(description: String, dt: Long) = String.format("${description.capitalize()} at ${getDayOfWeek(dt).capitalize()}" )

    private fun getDayOfWeek(dt: Long) = SimpleDateFormat("EEEE, HH:mm").format(Date(dt * 1000))

    fun speedFormat(length: Double, context: Context, type: String) =
            String.format("${lengthValue(context, type, length)} ${speedSymbol(type, context)}", length.toLong())

    fun smallLenghtFormat(length: Double, context: Context, type: String) =
            String.format("${lengthValue(context, type, length)} ${smallSymbol(type, context)}", length.toLong())

    private fun lengthValue(context: Context, type: String, length: Double) =
            when (context.resources.getStringArray(R.array.settings_length).indexOf(type)) {
                0 -> {
                    length.toInt()
                }
                1 -> {
                    (length / 25.4).toInt()
                }
                else -> {
                    length.toInt()
                }
            }

    private fun speedSymbol(type: String, context: Context): String {
        val lengthArray = context.resources.getStringArray(R.array.settings_length)
        val speedArray = context.resources.getStringArray(R.array.settings_length_speed)

        return speedArray[lengthArray.indexOf(type)]
    }

    private fun smallSymbol(type: String, context: Context): String {
        val lengthArray = context.resources.getStringArray(R.array.settings_length)
        val smallArray = context.resources.getStringArray(R.array.settings_length_small)

        return smallArray[lengthArray.indexOf(type)]
    }
}