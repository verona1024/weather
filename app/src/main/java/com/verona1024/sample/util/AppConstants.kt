package com.verona1024.sample.util

object AppConstants {

    internal val APP_DB_NAME = "weather_mvp.db"
    internal val PREF_NAME = "weather_pref"
}